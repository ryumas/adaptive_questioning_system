# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170110165330) do

  create_table "questions", force: :cascade do |t|
    t.integer  "qid"
    t.string   "field"
    t.text     "question"
    t.string   "answer"
    t.string   "dummy1"
    t.string   "dummy2"
    t.string   "dummy3"
    t.string   "dummy4"
    t.string   "dummy5"
    t.text     "note"
    t.float    "b"
    t.integer  "user1",      default: 0
    t.integer  "user2",      default: 0
    t.integer  "user3",      default: 0
    t.integer  "user4",      default: 0
    t.integer  "user5",      default: 0
    t.integer  "user6",      default: 0
    t.integer  "user7",      default: 0
    t.integer  "user8",      default: 0
    t.integer  "user9",      default: 0
    t.integer  "user10",     default: 0
    t.integer  "user11",     default: 0
    t.integer  "user12",     default: 0
    t.integer  "user13",     default: 0
    t.integer  "user14",     default: 0
    t.integer  "user15",     default: 0
    t.integer  "user16",     default: 0
    t.integer  "user17",     default: 0
    t.integer  "user18",     default: 0
    t.integer  "user19",     default: 0
    t.integer  "user20",     default: 0
    t.integer  "user21",     default: 0
    t.integer  "user22",     default: 0
    t.integer  "user23",     default: 0
    t.integer  "user24",     default: 0
    t.integer  "user25",     default: 0
    t.integer  "user26",     default: 0
    t.integer  "user27",     default: 0
    t.integer  "user28",     default: 0
    t.integer  "user29",     default: 0
    t.integer  "user30",     default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.boolean  "exist",           default: false
    t.integer  "number"
    t.string   "name"
    t.string   "password_digest"
    t.float    "theta",           default: 0.0
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

end

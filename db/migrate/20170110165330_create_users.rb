class CreateUsers < ActiveRecord::Migration[5.0]
	def change
		create_table :users do |t|

			t.boolean :exist,default: false

			t.integer :number

			t.string :name
			t.string :password_digest

			t.float :theta,default: 0.0

			t.timestamps
		end
	end
end
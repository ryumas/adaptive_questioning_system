class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|

        t.integer :qid
        t.string :field
        t.text :question
        t.string :answer
        t.string :dummy1
        t.string :dummy2
        t.string :dummy3
        t.string :dummy4
        t.string :dummy5
        t.text :note

        t.float :b

        t.integer :user1,default: 0
        t.integer :user2,default: 0
        t.integer :user3,default: 0
        t.integer :user4,default: 0
        t.integer :user5,default: 0
        t.integer :user6,default: 0
        t.integer :user7,default: 0
        t.integer :user8,default: 0
        t.integer :user9,default: 0
        t.integer :user10,default: 0
        t.integer :user11,default: 0
        t.integer :user12,default: 0
        t.integer :user13,default: 0
        t.integer :user14,default: 0
        t.integer :user15,default: 0
        t.integer :user16,default: 0
        t.integer :user17,default: 0
        t.integer :user18,default: 0
        t.integer :user19,default: 0
        t.integer :user20,default: 0
        t.integer :user21,default: 0
        t.integer :user22,default: 0
        t.integer :user23,default: 0
        t.integer :user24,default: 0
        t.integer :user25,default: 0
        t.integer :user26,default: 0
        t.integer :user27,default: 0
        t.integer :user28,default: 0
        t.integer :user29,default: 0
        t.integer :user30,default: 0

        t.timestamps
    end
  end
end
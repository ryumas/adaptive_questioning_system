# -*- coding: utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


#FE1612

@q1_qst="1.png"; @q1_ans="ウ"; @q1_dmy1="ア"; @q1_dmy2="イ"; @q1_dmy3="エ";
@q2_qst="2.png"; @q2_ans="エ"; @q2_dmy1="ア"; @q2_dmy2="イ"; @q2_dmy3="ウ";
@q3_qst="3.png"; @q3_ans="ア"; @q3_dmy1="イ"; @q3_dmy2="ウ"; @q3_dmy3="エ";
@q4_qst="4.png"; @q4_ans="エ"; @q4_dmy1="ア"; @q4_dmy2="イ"; @q4_dmy3="ウ";
@q5_qst="5.png"; @q5_ans="ア"; @q5_dmy1="イ"; @q5_dmy2="ウ"; @q5_dmy3="エ";
@q6_qst="6.png"; @q6_ans="イ"; @q6_dmy1="ア"; @q6_dmy2="ウ"; @q6_dmy3="エ";
@q7_qst="7.png"; @q7_ans="イ"; @q7_dmy1="ア"; @q7_dmy2="ウ"; @q7_dmy3="エ";
@q8_qst="8.png"; @q8_ans="ウ"; @q8_dmy1="ア"; @q8_dmy2="イ"; @q8_dmy3="エ";
@q9_qst="9.png"; @q9_ans="ア"; @q9_dmy1="イ"; @q9_dmy2="ウ"; @q9_dmy3="エ";
@q10_qst="10.png"; @q10_ans="エ"; @q10_dmy1="ア"; @q10_dmy2="イ"; @q10_dmy3="ウ";
@q11_qst="11.png"; @q11_ans="ウ"; @q11_dmy1="ア"; @q11_dmy2="イ"; @q11_dmy3="エ";
@q12_qst="12.png"; @q12_ans="ア"; @q12_dmy1="イ"; @q12_dmy2="ウ"; @q12_dmy3="エ";
@q13_qst="13.png"; @q13_ans="ウ"; @q13_dmy1="ア"; @q13_dmy2="イ"; @q13_dmy3="エ";
@q14_qst="14.png"; @q14_ans="エ"; @q14_dmy1="ア"; @q14_dmy2="イ"; @q14_dmy3="ウ";
@q15_qst="15.png"; @q15_ans="エ"; @q15_dmy1="ア"; @q15_dmy2="イ"; @q15_dmy3="ウ";
@q16_qst="16.png"; @q16_ans="ウ"; @q16_dmy1="ア"; @q16_dmy2="イ"; @q16_dmy3="エ";
@q17_qst="17.png"; @q17_ans="ア"; @q17_dmy1="イ"; @q17_dmy2="ウ"; @q17_dmy3="エ";
@q18_qst="18.png"; @q18_ans="ウ"; @q18_dmy1="ア"; @q18_dmy2="イ"; @q18_dmy3="エ";
@q19_qst="19.png"; @q19_ans="ア"; @q19_dmy1="イ"; @q19_dmy2="ウ"; @q19_dmy3="エ";
@q20_qst="20.png"; @q20_ans="イ"; @q20_dmy1="ア"; @q20_dmy2="ウ"; @q20_dmy3="エ";
@q21_qst="21.png"; @q21_ans="ウ"; @q21_dmy1="ア"; @q21_dmy2="イ"; @q21_dmy3="エ";
@q22_qst="22.png"; @q22_ans="エ"; @q22_dmy1="ア"; @q22_dmy2="イ"; @q22_dmy3="ウ";
@q23_qst="23.png"; @q23_ans="ア"; @q23_dmy1="イ"; @q23_dmy2="ウ"; @q23_dmy3="エ";
@q24_qst="24.png"; @q24_ans="エ"; @q24_dmy1="ア"; @q24_dmy2="イ"; @q24_dmy3="ウ";
@q25_qst="25.png"; @q25_ans="ウ"; @q25_dmy1="ア"; @q25_dmy2="イ"; @q25_dmy3="エ";
@q26_qst="26.png"; @q26_ans="イ"; @q26_dmy1="ア"; @q26_dmy2="ウ"; @q26_dmy3="エ";
@q27_qst="27.png"; @q27_ans="ア"; @q27_dmy1="イ"; @q27_dmy2="ウ"; @q27_dmy3="エ";
@q28_qst="28.png"; @q28_ans="ア"; @q28_dmy1="イ"; @q28_dmy2="ウ"; @q28_dmy3="エ";
@q29_qst="29.png"; @q29_ans="ウ"; @q29_dmy1="ア"; @q29_dmy2="イ"; @q29_dmy3="エ";
@q30_qst="30.png"; @q30_ans="エ"; @q30_dmy1="ア"; @q30_dmy2="イ"; @q30_dmy3="ウ";
@q31_qst="31.png"; @q31_ans="エ"; @q31_dmy1="ア"; @q31_dmy2="イ"; @q31_dmy3="ウ";
@q32_qst="32.png"; @q32_ans="イ"; @q32_dmy1="ア"; @q32_dmy2="ウ"; @q32_dmy3="エ";
@q33_qst="33.png"; @q33_ans="イ"; @q33_dmy1="ア"; @q33_dmy2="ウ"; @q33_dmy3="エ";
@q34_qst="34.png"; @q34_ans="イ"; @q34_dmy1="ア"; @q34_dmy2="ウ"; @q34_dmy3="エ";
@q35_qst="35.png"; @q35_ans="イ"; @q35_dmy1="ア"; @q35_dmy2="ウ"; @q35_dmy3="エ";
@q36_qst="36.png"; @q36_ans="ア"; @q36_dmy1="イ"; @q36_dmy2="ウ"; @q36_dmy3="エ";
@q37_qst="37.png"; @q37_ans="ア"; @q37_dmy1="イ"; @q37_dmy2="ウ"; @q37_dmy3="エ";
@q38_qst="38.png"; @q38_ans="ア"; @q38_dmy1="イ"; @q38_dmy2="ウ"; @q38_dmy3="エ";
@q39_qst="39.png"; @q39_ans="ウ"; @q39_dmy1="ア"; @q39_dmy2="イ"; @q39_dmy3="エ";
@q40_qst="40.png"; @q40_ans="エ"; @q40_dmy1="ア"; @q40_dmy2="イ"; @q40_dmy3="ウ";
@q41_qst="41.png"; @q41_ans="イ"; @q41_dmy1="ア"; @q41_dmy2="ウ"; @q41_dmy3="エ";
@q42_qst="42.png"; @q42_ans="ア"; @q42_dmy1="イ"; @q42_dmy2="ウ"; @q42_dmy3="エ";
@q43_qst="43.png"; @q43_ans="エ"; @q43_dmy1="ア"; @q43_dmy2="イ"; @q43_dmy3="ウ";
@q44_qst="44.png"; @q44_ans="ア"; @q44_dmy1="イ"; @q44_dmy2="ウ"; @q44_dmy3="エ";
@q45_qst="45.png"; @q45_ans="ウ"; @q45_dmy1="ア"; @q45_dmy2="イ"; @q45_dmy3="エ";
@q46_qst="46.png"; @q46_ans="エ"; @q46_dmy1="ア"; @q46_dmy2="イ"; @q46_dmy3="ウ";
@q47_qst="47.png"; @q47_ans="エ"; @q47_dmy1="ア"; @q47_dmy2="イ"; @q47_dmy3="ウ";
@q48_qst="48.png"; @q48_ans="エ"; @q48_dmy1="ア"; @q48_dmy2="イ"; @q48_dmy3="ウ";
@q49_qst="49.png"; @q49_ans="ウ"; @q49_dmy1="ア"; @q49_dmy2="イ"; @q49_dmy3="エ";
@q50_qst="50.png"; @q50_ans="エ"; @q50_dmy1="ア"; @q50_dmy2="イ"; @q50_dmy3="ウ";
@q51_qst="51.png"; @q51_ans="ア"; @q51_dmy1="イ"; @q51_dmy2="ウ"; @q51_dmy3="エ";
@q52_qst="52.png"; @q52_ans="ア"; @q52_dmy1="イ"; @q52_dmy2="ウ"; @q52_dmy3="エ";
@q53_qst="53.png"; @q53_ans="イ"; @q53_dmy1="ア"; @q53_dmy2="ウ"; @q53_dmy3="エ";
@q54_qst="54.png"; @q54_ans="エ"; @q54_dmy1="ア"; @q54_dmy2="イ"; @q54_dmy3="ウ";
@q55_qst="55.png"; @q55_ans="ア"; @q55_dmy1="イ"; @q55_dmy2="ウ"; @q55_dmy3="エ";
@q56_qst="56.png"; @q56_ans="イ"; @q56_dmy1="ア"; @q56_dmy2="ウ"; @q56_dmy3="エ";
@q57_qst="57.png"; @q57_ans="エ"; @q57_dmy1="ア"; @q57_dmy2="イ"; @q57_dmy3="ウ";
@q58_qst="58.png"; @q58_ans="ア"; @q58_dmy1="イ"; @q58_dmy2="ウ"; @q58_dmy3="エ";
@q59_qst="59.png"; @q59_ans="イ"; @q59_dmy1="ア"; @q59_dmy2="ウ"; @q59_dmy3="エ";
@q60_qst="60.png"; @q60_ans="ア"; @q60_dmy1="イ"; @q60_dmy2="ウ"; @q60_dmy3="エ";
@q61_qst="61.png"; @q61_ans="ウ"; @q61_dmy1="ア"; @q61_dmy2="イ"; @q61_dmy3="エ";
@q62_qst="62.png"; @q62_ans="エ"; @q62_dmy1="ア"; @q62_dmy2="イ"; @q62_dmy3="ウ";
@q63_qst="63.png"; @q63_ans="ア"; @q63_dmy1="イ"; @q63_dmy2="ウ"; @q63_dmy3="エ";
@q64_qst="64.png"; @q64_ans="イ"; @q64_dmy1="ア"; @q64_dmy2="ウ"; @q64_dmy3="エ";
@q65_qst="65.png"; @q65_ans="ウ"; @q65_dmy1="ア"; @q65_dmy2="イ"; @q65_dmy3="エ";
@q66_qst="66.png"; @q66_ans="ウ"; @q66_dmy1="ア"; @q66_dmy2="イ"; @q66_dmy3="エ";
@q67_qst="67.png"; @q67_ans="エ"; @q67_dmy1="ア"; @q67_dmy2="イ"; @q67_dmy3="ウ";
@q68_qst="68.png"; @q68_ans="エ"; @q68_dmy1="ア"; @q68_dmy2="イ"; @q68_dmy3="ウ";
@q69_qst="69.png"; @q69_ans="ア"; @q69_dmy1="イ"; @q69_dmy2="ウ"; @q69_dmy3="エ";
@q70_qst="70.png"; @q70_ans="イ"; @q70_dmy1="ア"; @q70_dmy2="ウ"; @q70_dmy3="エ";
@q71_qst="71.png"; @q71_ans="エ"; @q71_dmy1="ア"; @q71_dmy2="イ"; @q71_dmy3="ウ";
@q72_qst="72.png"; @q72_ans="ウ"; @q72_dmy1="ア"; @q72_dmy2="イ"; @q72_dmy3="エ";
@q73_qst="73.png"; @q73_ans="エ"; @q73_dmy1="ア"; @q73_dmy2="イ"; @q73_dmy3="ウ";
@q74_qst="74.png"; @q74_ans="エ"; @q74_dmy1="ア"; @q74_dmy2="イ"; @q74_dmy3="ウ";
@q75_qst="75.png"; @q75_ans="ウ"; @q75_dmy1="ア"; @q75_dmy2="イ"; @q75_dmy3="エ";
@q76_qst="76.png"; @q76_ans="イ"; @q76_dmy1="ア"; @q76_dmy2="ウ"; @q76_dmy3="エ";
@q77_qst="77.png"; @q77_ans="イ"; @q77_dmy1="ア"; @q77_dmy2="ウ"; @q77_dmy3="エ";
@q78_qst="78.png"; @q78_ans="エ"; @q78_dmy1="ア"; @q78_dmy2="イ"; @q78_dmy3="ウ";
@q79_qst="79.png"; @q79_ans="ア"; @q79_dmy1="イ"; @q79_dmy2="ウ"; @q79_dmy3="エ";
@q80_qst="80.png"; @q80_ans="ア"; @q80_dmy1="イ"; @q80_dmy2="ウ"; @q80_dmy3="エ";

random=Random.new
1.upto(80) do |no|
	Question.create(:qid=>no,:question=>eval("@q#{no}_qst"),:answer=>eval("@q#{no}_ans"),
		:dummy1=>eval("@q#{no}_dmy1"),:dummy2=>eval("@q#{no}_dmy2"),:dummy3=>eval("@q#{no}_dmy3"),
		:b=>random.rand(-3.0..3.0),
		:user1=>random.rand(2),
		:user2=>random.rand(2),
		:user3=>random.rand(2),
		:user4=>random.rand(2),
		:user5=>random.rand(2),
		:user6=>random.rand(2),
		:user7=>random.rand(2),
		:user8=>random.rand(2),
		:user9=>random.rand(2),
		:user10=>random.rand(2),
		:user11=>random.rand(2),
		:user12=>random.rand(2),
		:user13=>random.rand(2),
		:user14=>random.rand(2),
		:user15=>random.rand(2),
		:user16=>random.rand(2),
		:user17=>random.rand(2),
		:user18=>random.rand(2),
		:user19=>random.rand(2),
		:user20=>random.rand(2),
		:user21=>random.rand(2),
		:user22=>random.rand(2),
		:user23=>random.rand(2),
		:user24=>random.rand(2),
		:user25=>random.rand(2),
		:user26=>random.rand(2),
		:user27=>random.rand(2),
		:user28=>random.rand(2),
		:user29=>random.rand(2),
		:user30=>random.rand(2)
		)
end

=begin
1.upto(1) do |no|
	User.create(:name=>"hogehoge"+no.to_s,:password=>"fugafuga"+no.to_s,:number=>no,
		:theta=>random.rand(-3.0..3.0)
		)
end
=end
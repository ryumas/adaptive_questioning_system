# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


つかいかた(ver.1.0)  
ディレクトリ内でコマンド「rails s」でサーバ起動→「<http://localhost:3000/questions/show>」にアクセス  
正解不正解表示とかまだ出ません（Ajaxが使えていない）  
  
コントローラ：adaptive_questioning_system / app / controllers / questions_controller.rb  
ビュー：adaptive_questioning_system / app / views / questions / show.html.erb  
（以上が本体）  
questionsマイグレーションファイル： adaptive_questioning_system / db / migrate / 20160824133610_create_questions.rb  
usersマイグレーションファイル： adaptive_questioning_system / db / migrate / 20170110165330_create_users.rb  
seedsファイル（一部スクリプトで作成）： adaptive_questioning_system / db / seeds.rb

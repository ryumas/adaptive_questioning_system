class RegistersController < ApplicationController
	def index
		render "index"
	end

	def show
		render "new"
	end

	def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if User.find_by(name: @user.name) == nil && @user.save
      @user.number = @user.id
      @user.save
      redirect_to root_path, notice: "ユーザ登録しました。"
    else
      render 'new'
    end
  end

  private

  def user_params
   params.require(:user).permit(:name, :password)
 end
end
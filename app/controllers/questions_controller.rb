class QuestionsController < ApplicationController

  include AjaxHelper

  @@answered=false
  @@reload_status='next'
  @@question_set=Question.new

  def index
  end

  def show
    @@answered=false
    @@questions=Question.all
    @num_of_questions=@@questions.length

    @@users=User.where(exist: true)

    #@question_set||=@@questions.sample
    if @@reload_status=='next'
      margin_max=Float::MAX
      @@questions.each do |q|
        if (current_user.theta-q.b).abs<margin_max
          margin_max=(current_user.theta-q.b).abs
          @@question_set=q
        end
      end
      @question=@@question_set
    else
      @question=@@questions.sample
    end

    collects=0
    @@questions.each do |q|
      collects+=eval("q.user#{current_user.number}")
    end
    @accuracy=collects/@@questions.length.to_f
  end

  def ajax_method
    if params[:reload].present?
      @@reload_status=params[:reload]
    end
    respond_to do |format|
      format.js{render ajax_redirect_to "http://localhost:3000/questions/show"}
    end
  end

  def button
    if params[:chosen].present?&&!@@answered
      if params[:chosen]==params[:answer]
        @result=1
      else
        @result=0
      end
      @@question_set.update("user#{current_user.number}": @result)
      @number_chosen=params[:btnid]
      @@answered=true
      current_user.exist=true
      current_user.save

      l_theta=Float::MIN
      -3.0.step(3.0,0.1) do |theta_arg|
        tmp_calc=1.0
        @@questions.each do |q|
          tmp_calc*=(1.0/(1.0+Math.exp(-1.7*(theta_arg-q.b))))**eval("q.user#{current_user.number}")*(1.0-1.0/(1.0+Math.exp(-1.7*(theta_arg-q.b))))**(1.0-eval("q.user#{current_user.number}"))
        end
        if l_theta<tmp_calc
          current_user.theta=theta_arg
          l_theta=tmp_calc
        end
      end
      current_user.save
      p "updated theta of user#{current_user.number}: "+current_user.theta.to_s
      l_b=Float::MIN
      -3.0.step(3.0,0.1) do |b_arg|
        tmp_calc=1.0
        @@users.each do |u|
          tmp_calc*=(1.0/(1.0+Math.exp(-1.7*(u.theta-b_arg))))**eval("@@question_set.user#{u.number}")*(1.0-1.0/(1.0+Math.exp(-1.7*(u.theta-b_arg))))**(1.0-eval("@@question_set.user#{u.number}"))
        end
        if l_b<tmp_calc
          @@question_set.b=b_arg
          l_b=tmp_calc
        end
      end
      @@question_set.save
      p "updated b of questions#{@@question_set.qid}: "+@@question_set.b.to_s

      f=File.open('./db/record.txt', 'w')
      record=""
      @@users.each do |u|
        @@questions.each do |q|
          record+="u_"+u.number.to_s+"_"+q.qid.to_s+"="+eval("q.user#{u.number}").to_s+";"
        end
      end
      f.print encrypt(record)
      f.close
    end
  end

  def encrypt(word)
    crypt = ActiveSupport::MessageEncryptor.new(SECURE, CIPHER)
    crypt.encrypt_and_sign(word)
  end

  def decrypt(word)
    crypt = ActiveSupport::MessageEncryptor.new(SECURE, CIPHER)
    crypt.decrypt_and_verify(word)
  end
end
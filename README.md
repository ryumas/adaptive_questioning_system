# README

* つかいかた(ver.1.2)  
コマンド「rake db:migrate:reset」でデータベース作成、「rake db:seed」で初期データ投入  
ディレクトリ内でコマンド「rails s」でサーバ起動→「<http://localhost:3000>」にアクセス
（現在ターミナル上でのみパラメータ更新値の確認が可能）  
  
* コントローラ：adaptive_questioning_system / app / controllers / questions_controller.rb  
* ビュー：adaptive_questioning_system / app / views / questions / show.html.erb  
（以上が本体）  
* questionsマイグレーションファイル： adaptive_questioning_system / db / migrate / 20160824133610_create_questions.rb  
* usersマイグレーションファイル： adaptive_questioning_system / db / migrate / 20170110165330_create_users.rb  
* seedsファイル（一部スクリプトで作成）： adaptive_questioning_system / db / seeds.rb

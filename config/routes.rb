Rails.application.routes.draw do
	get 'questions/index'

	get 'questions/show'

	get 'questions/ajax_method'=>'questions#ajax_method',as:'ajax_method'

	get 'questions/button'=>'questions#button',as:'button'

	root 'logins#index'
	resource :login, only: %i{create destroy} 
  get 'login', to: 'logins#new'

	post 'register' => 'registers#create'
  get 'register', to: 'registers#new'

  resources :users

  #rake routes

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end